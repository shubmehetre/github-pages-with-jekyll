---
title: "my-first-blog-post"
date: 2021-06-12
---
## History of Linux
	
  Unix is the ancestor of linux. Unix was created by Dennis Ritchie and Ken Thomson at Bell Labs. Its was first modern OS which was portable and easy to use. Because it was commercially licensed, a Finnish engineer Linus Torvalds thought of creating a true free and open-source OS. So he developed the a Kernel and named it Linux. Combining with the then ongoing GNU project, it formed a full fledged free and opensource Operating System - GNU/Linux or simply Linux.
	
- Major Families of Linux distributions
	- Debian
	- Red Hat
	- OpenSuse
	- Arch
	- Gentoo
	Later in 2004, a group of developers created Ubuntu which was derived from Debian. Ubuntu was very easy to install and use, so became and still is most used distro.

shells: 
	- A shell is an interface via which commands can be inputted to the kernel. It is called shell because its the outermost layer in our OS's structure.
	- It exposes OS services to humans or any other programs
	- In general, operating system shells use:
		- Command-Line Interface (CLI)
		- Graphical User Interface(GUI)
2 types of shells:
- CLI: 
	- A command-line interface is a way of sending instructions and data to the OS/kernel/services using alphanumeric keystrokes through a keyboard on a prompt. 
	- These command have syntax depending on the OS or application we trying to use.
	- We can write scripts using shell based scripting languages like bash using a CLI
	- eg. tty on unix
- GUI:
	- A graphical user interface is a way of sending instructions and doing tasks through a graphical windows using a mouse clicks.
	- It is easier and beginner friendly because we dont need to remember any syntax
	- We can manipulate stuff graphically on different windows for different programs
	- Windows 10 GUI
	- 
